# desafio-product-owner

Desafio proposto para os candidatos para a vaga de Product Owner/Manager (também para vagas de Business Analyst) da BossaBox.

**O que esperamos do P.O.?**
===================================

A partir do Kick-Off e durante o desenvolvimento do produto utilizando a metodologia Agile , o PO é o responsável principal por conduzir dinâmicas para alcançar o entendimento compartilhado sobre o produto no qual está trabalhando. 
Dessa forma, haverá clareza sobre quais são os objetivos a serem alcançados e as prioridades do desenvolvimento que deverá ser realizado pelo time de design e engenharia entre todos os Stakeholders do projeto.
Também caberá ao Product Owner definir todas raízes do produto, construindo-as a partir das informações que conseguir coletar do cliente, assim como através de pesquisas e reflexões realizadas durante a Descoberta do produto.

Em suma: o Product Owner, como "Dono do Produto", deve garantir, simultaneamente: (1) a maximização de valor criado para os usuários do produto, mantendo (2) um alto nível de satisfação do cliente com os resultados obtidos e o serviço prestado e (3) assegurando que o *squad* inteiro esteja com plena compreensão do que deve ser feito e com um alto nível de engajamento com o trabalho.

**O P.O. é o Dono do Backlog:**

Organização contínua dos Backlogs : o desenvolvimento de um produto digital exige muito cuidado com a organização do que precisa ser feito por todos os Stakeholders, sendo muito fácil os membros da equipe esquecerem detalhes que foram discutidos durante dinâmicas, pesquisas, etc. Para facilitar o seu próprio trabalho e para criar um entendimento mais fácil do direcionamento estratégico do produto, o PO deve garantir a qualidade de dois backlogs paralelos e de forma contínua: 

Backlog do Produto: Objectives, Epics (passos), User Stories e as suas Releases.
    As User Stories devem ser priorizadas em termo de valor criado para o usuário dado que, ao término da Sprint em que for contemplada, ela irá incrementar a narrativa dessa persona durante o uso do produto.
    As User Stories devem ser estimadas em termos de pontos, seguindo a sequência de Fibonacci: 1, 2, 3, 5, 8, 13, 21 (caso uma User Story esteja com 21 pontos, o P.O. deve atuar para investiga-la em quebrar em USs menores), durante o refinamento do backlog juntamente com o Time. *Sugestão: atribuir "1" à User Story mais fácil e "21" à mais difícil; com isso, fica mais fácil para todos estimarem relativamente a esses mínimos e um máximos determinados.

Backlog da Sprint: User Stories de uma determinada Sprint, quebradas em Tarefas e Sub-Tarefas.
    De acordo com a Sprint Planning - levando em conta Squad Velocity, Product Backlog, etc - forma-se o Sprint Backlog. Essas User Stories - cujo objetivo é mover de "Ready" para "Done" durante a Sprint - devem ser quebradas em Tarefas e Sub-Tarefas (caso as tarefas precisem ser quebradas entre diferentes membros ou for ser melhor explicada mediante sua partição em sub-tarefas) que o Time deve se comprometer com prazos para entrega, de acordo com uma estimativa de esforço em Dias ou Horas. Pela primeira vez até aqui, a variável tempo surgiu no Backlog - até então, estava restrita para o planejamento do Release total dessa versão.
    
    
**O Desafio: Musikfy**
==========================
**Benchmark de Negócio:** O produto que o nosso cliente quer construir é uma versão da aplicação **SoundBetter** para o Brasil: https://soundbetter.com/. Uma versão "tropicalizada" da startup americana.  
**Benchmark de Estilo:** De acordo com as preferências dos fundadores e algumas análises de mercado previamente feitas, o estilo a ser empregado pela marca segue a linha da startup Lookback.io (https://lookback.io/).

A empresa já validou o modelo de negócios offline - com amigos, família e 10 clientes. Conseguiram um Round Seed e agora querem digitalizar a operação. 
Querem monetizar o produto e adquirir uma escala para ter 1.000 negócios fechados pelo produto no próximo ano.
A partir disso, expandir a operação para todo o Brasil e, em 5 anos, para a América Latina.

Você irá realizar a etapa inicial de de **Descoberta do Produto** (escopagem, entendimento dos objetivos, definição de guidelines)
A sua justificativa deve ser embasada com uma análise que contemple, simultaneamente: (1) viabilidade técnica, (2) análise de negócios (perfil de usuários, ecossistema do setor, benchmarks e concorrentes) e (3) planejamento e Squad necessário, assim como sugestão das Stacks de Design e Tecnologia necessárias - inclusive integrações e serviços externos já existentes que barateiem a construção da versão alpha (MVP) do cliente para maximizar o Retorno Sobre Investimento (ROI) dos recursos que está dedicando ao produto.

Com a discussão que ocorrerá no Kick-Off com o cliente, assim como com a coleta de inputs mediante o aceite da proposta comercial realizada pela BossaBox, certamente haverá ideias que serão despriorizadas, outras serão adicionadas, etc. Enfim, o que queremos saber é a sua capacidade de pensar em Negócios (estratégia de mercado/roadmap de produto/sugestão de funcionalidades), Design (lógica da aplicação e entendimento dos usuários) e Tecnologia (sugestão de ferramentas, serviços e stacks a serem utilizados) - nessa ordem de prioridade.

Entregáveis esperados para o trabalho:
====================================
- Sugestão de equipe necessária para realizar o trabalho
- Sugestão de RoadMap de produto - overview sobre os Releases (MVP, Release 1, Release 2). Quais grandes funcionalidades devem estar contempladas em cada um deles e a respectiva justificativa. Sugestão: utilizar o framework de priorização MoSCoW. A partir disso, segmentar releases.
- Versão inicial do StoryMap (formato de Product Backlog utilizado na Bossa). Conteúdo: https://drive.google.com/open?id=1Co6f_C2563SoMz3GnMUbCGCSIMnPywXO
- Certo refinamento do Backlog presente no StoryMap. Apontando dúvidas pertinentes, sugerindo uma série de User Stories baseando-se no esclarecimento dessas dúvidas - ordem de prioridade, de dificuldade, etc.

Ferramentas sugeridas para o trabalho:
====================================
- Pacote Office/Pacote Google
- Trello (Planejamento)
- Real Time Board (Documentação, no geral)
- XMind (Mindmapping)
- Lucidchart (Fluxogramas, no geral)
- Balsamiq (Wireframing)
- Flowmapp (User Flow, SiteMapping)
- Ou qualquer outra que julgar conveniente.


O candidato deve sugerir marcar uma ligação com a equipe da BossaBox, quando estiver pronto para apresentar a resposta do desafio deve marcar uma call com André de Castro Abreu (Diretor Executivo da BossaBox) em: https://andrebbox.youcanbook.me.


Qualquer sugestão/comentário/dúvida com relação ao processo, devem contatar diretamente através de: andre@bossabox.com.
